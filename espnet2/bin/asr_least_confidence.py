#!/usr/bin/env python3
import argparse
import logging
from pathlib import Path
import os
import sys
import time
from datetime import timedelta
from typing import Optional
from typing import Sequence
from typing import Tuple
from typing import Union

import numpy as np
import torch
from typeguard import check_argument_types
from typeguard import check_return_type
from typing import List

from espnet.utils.cli_utils import get_commandline_args
from espnet2.tasks.asr import ASRTask
from espnet2.torch_utils.set_all_random_seed import set_all_random_seed
from espnet2.bin.asr_inference import Speech2Text
from espnet2.utils import config_argparse
from espnet2.utils.types import str2bool
from espnet2.utils.types import str2triple_str
from espnet2.utils.types import str_or_none

"""
    asr_loss_predictor.py
        -- predict the least confidence score
        -- location: espent/espnet2/bin/asr_loss_predictor.py
"""

def init_writer(folder: str, fnames: list) -> None:
    """
        create file to save loss
    """
    assert check_argument_types()
    # make folder
    if not os.path.exists(folder):
        os.mkdir(folder)
    # init file:
    for fname in fnames:
        with open(os.path.join(folder, fname), "w") as file:
            pass
        # close


def writer(path: str, key: str, value: str, lang: str = 'en') -> None:
    """
        save file
    """
    assert check_argument_types()
    assert os.path.exists(path)

    if lang == 'en':
        with open(path, 'a') as file:
            file.write(" ".join([key, value]))
            file.write("\n")
        # close
    else:
        with open(path, 'a', encoding="utf-8") as file:
            file.write(" ".join([key, value]))
            file.write("\n")
        # close


def get_data_szie(path):
    assert os.path.exists(path)
    with open(path, 'r') as file:
        length = len(file.readlines())
    return length


def loss_predictor(
        output_dir: str,
        maxlenratio: float,
        minlenratio: float,
        batch_size: int,
        dtype: str,
        beam_size: int,
        ngpu: int,
        seed: int,
        ctc_weight: float,
        penalty: float,
        nbest: int,
        num_workers: int,
        log_level: Union[int, str],
        data_path_and_name_and_type: Sequence[Tuple[str, str, str]],
        key_file: Optional[str],
        asr_train_config: str,
        asr_model_file: str,
        token_type: Optional[str],
        bpemodel: Optional[str],
        allow_variable_data_keys: bool,
):
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s (%(module)s:%(lineno)d) %(levelname)s: %(message)s",
    )

    if ngpu > 1:
        logging.warning("cannot support multi-GPU")
        assert ngpu <= 1
    elif ngpu == 1:
        device = "cuda"
        logging.info("using cuda backend")
    else:
        device = "cpu"
        torch.backends.cudnn.enabled = False
        logging.info("using cpu backend")

    data_size = get_data_szie(key_file)

    # only test for nbest = 1
    assert nbest ==1


    # 1. Set random-seed
    set_all_random_seed(0)

    # 2. Build speech2text (inference)
    speech2text = Speech2Text(
        asr_train_config=asr_train_config,  # conf/train_asr_transformer
        asr_model_file=asr_model_file,  # exp/asr_train_asr_transofrmer_raw_char/valid.acc.ave.pth
        token_type=token_type,  # char
        bpemodel=bpemodel,  # None
        device=device,
        maxlenratio=maxlenratio,
        minlenratio=minlenratio,
        dtype=dtype,  # float32
        beam_size=beam_size,  # 20
        ctc_weight=ctc_weight,  # 0.3
        penalty=penalty,
        nbest=nbest,  # 1
    )


    # 4. build data-iterator
    loader = ASRTask.build_streaming_iterator(
        data_path_and_name_and_type,
        dtype=dtype,
        batch_size=batch_size,
        key_file=key_file,
        num_workers=num_workers,
        preprocess_fn=ASRTask.build_preprocess_fn(speech2text.asr_train_args, False),
        collate_fn=ASRTask.build_collate_fn(speech2text.asr_train_args, False),
        allow_variable_data_keys=allow_variable_data_keys,
        inference=True,
    )

    # main loop
    # with DatadirWriter(output_dir) as writer:
    init_writer(output_dir, ["least_confidence", "tokenint", "text"])
    count = 0
    start_time = time.perf_counter()
    avg_seconds = 0
    # memory detect
    # pid = os.getpid()
    for keys, batch in loader:
        iteration_time = time.perf_counter()
        count += 1

        assert isinstance(batch, dict), type(batch)
        assert all(isinstance(s, str) for s in keys), keys
        _bs = len(next(iter(batch.values())))
        assert len(keys) == _bs, f"{len(keys)} != {_bs}"
        batch = {k: v[0] for k, v in batch.items() if not k.endswith("_lengths")}

        # only support batch size = 1
        key = keys[0]

        # cur_mem = (int(open('/proc/%s/statm' % pid, 'r').read().split()[1]) + 0.0) / 256
        # N-best list of (text, token, token_int, hyp_object)
        results = speech2text(**batch)

        # only need best inference
        result = results[0][3]

        # get confidence score (probability)
        log_confid = result.scores['ctc'].item()
        norm_log_confid = log_confid / len(result.yseq)

        # writer results into files
        writer(path=os.path.join(output_dir, "least_confidence"), key=key, value=str(norm_log_confid))
        writer(path=os.path.join(output_dir, "tokenint"), key=key, value=" ".join(map(str, results[0][2])))
        writer(path=os.path.join(output_dir, "text"), key=key, value=results[0][0], lang='cn')

        # calculate remaining time to finish prediction process
        iteration_duration = np.round(time.perf_counter() - iteration_time, 1)
        avg_seconds = avg_seconds + iteration_duration
        rst_time = str(timedelta(seconds=(data_size - count) * (avg_seconds / count))).split(".")[
            0]  # num of iteration * (average time / iteration)
        total_duration = str(timedelta(seconds=(time.perf_counter() - start_time))).split(".")[0]
        logging.info("{} iteration {} takes {} seconds, total costs {}, rest time {} \n"
                     .format(str(count), key, str(iteration_duration), total_duration, rst_time))

        # manually clear memory
        del key, batch
        del results, result, log_confid, norm_log_confid
        del iteration_duration, rst_time, total_duration


def get_parser():
    parser = config_argparse.ArgumentParser(
        description="ASR Decoding",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    # Note(kamo): Use '_' instead of '-' as separator.
    # '-' is confusing if written in yaml.
    parser.add_argument(
        "--log_level",
        type=lambda x: x.upper(),
        default="INFO",
        choices=("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"),
        help="The verbose level of logging",
    )

    parser.add_argument("--output_dir", type=str, required=True)
    parser.add_argument(
        "--ngpu",
        type=int,
        default=0,
        help="The number of gpus. 0 indicates CPU mode",
    )
    parser.add_argument("--seed", type=int, default=0, help="Random seed")
    parser.add_argument(
        "--dtype",
        default="float32",
        choices=["float16", "float32", "float64"],
        help="Data type",
    )
    parser.add_argument(
        "--num_workers",
        type=int,
        default=1,
        help="The number of workers used for DataLoader",
    )

    group = parser.add_argument_group("Input data related")
    group.add_argument(
        "--data_path_and_name_and_type",
        type=str2triple_str,
        required=True,
        action="append",
    )
    group.add_argument("--key_file", type=str_or_none)
    group.add_argument("--allow_variable_data_keys", type=str2bool, default=False)

    group = parser.add_argument_group("The model configuration related")
    group.add_argument("--asr_train_config", type=str, required=True)
    group.add_argument("--asr_model_file", type=str, required=True)

    group = parser.add_argument_group("Beam-search related")
    group.add_argument(
        "--batch_size",
        type=int,
        default=1,
        help="The batch size for inference",
    )
    group.add_argument("--nbest", type=int, default=1, help="Output N-best hypotheses")
    group.add_argument("--beam_size", type=int, default=20, help="Beam size")
    group.add_argument("--penalty", type=float, default=0.0, help="Insertion penalty")
    group.add_argument(
        "--maxlenratio",
        type=float,
        default=0.0,
        help="Input length ratio to obtain max output length. "
             "If maxlenratio=0.0 (default), it uses a end-detect "
             "function "
             "to automatically find maximum hypothesis lengths",
    )
    group.add_argument(
        "--minlenratio",
        type=float,
        default=0.0,
        help="Input length ratio to obtain min output length",
    )
    group.add_argument(
        "--ctc_weight",
        type=float,
        default=0.3,
        help="CTC weight in joint decoding",
    )

    group = parser.add_argument_group("Text converter related")
    group.add_argument(
        "--token_type",
        type=str_or_none,
        default="char",
        choices=["char", "bpe", None],
        help="The token type for ASR model. "
             "If not given, refers from the training args",
    )
    group.add_argument(
        "--bpemodel",
        type=str_or_none,
        default=None,
        help="The model path of sentencepiece. "
             "If not given, refers from the training args",
    )

    return parser


def main(cmd=None):
    print(get_commandline_args(), file=sys.stderr)
    parser = get_parser()
    args = parser.parse_args(cmd)
    kwargs = vars(args)
    kwargs.pop("config", None)
    loss_predictor(**kwargs)


if __name__ == "__main__":
    main()
