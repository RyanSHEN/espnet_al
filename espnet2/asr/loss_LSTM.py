import torch.nn as nn
import torch.nn.functional as F


"""
   loss_LSTM.py  
    -- object: define the LSTM for loss prediction LP module
    -- location: espent/espnet2/asr/loss_LSTM.py
    -- description:
            -- 2-layer LSTM
            -- 2-layer feedforward
    
"""

class lossLSTM(nn.Module):
    
    def __init__(self, hidden, fc_hidden):
        super(lossLSTM, self).__init__()
        self.LSTM = nn.LSTM(
                        input_size=256,
                        hidden_size=hidden,
                        num_layers=2,
                        batch_first=True,
                        dropout=0.1
                    )
        self.FC1 = nn.Linear(hidden, fc_hidden)
        self.FC2 = nn.Linear(fc_hidden, 1)

    def forward(self, input, hprev, cprev):
        output, (h_n, c_n) = self.LSTM(input, (hprev, cprev))
        # only need last chuck and reshape
        output = output[:, -1, :]
        output = output.view(output.data.shape[0], output.data.shape[1])
        output = F.relu(self.FC1(output))
        output = F.relu(self.FC2(output))
        output = output.view(output.data.shape[0])
        return output, h_n, c_n
