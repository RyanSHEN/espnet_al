#!/bin/bash

# active python env

run_start=1

log() {
    local fname=${BASH_SOURCE[1]##*/}
    echo -e "$(date '+%Y-%m-%dT%H:%M:%S') (${fname}:${BASH_LINENO[0]}:${FUNCNAME[1]}) $*"
}

for i in $(seq 5);do
  if test $i -ge $run_start
  then
      #Todo
      filename="./asr_al_random0.${i}"
      log "begin at ${filename}"
      cd $filename || exit

          pwd
          sleep 5

          printf "train data size:  " && wc -l dump/raw/train/wav.scp

          if [ -f ./dump/raw/train/wav.scp ]
          then
             log "run training"
             CUDA_VISIBLE_DEVICES=2,3 bash run.sh
          else
            log "cannot find wav.scp"
            exit
          fi

      cd ../

      log "finish one iteration"
      echo
      sleep 5
  fi
done