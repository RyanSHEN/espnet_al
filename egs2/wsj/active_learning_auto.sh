#!/bin/bash

# active python env


copy_start=2
run_start=1

# shellcheck disable=SC2120
log() {
    local fname=${BASH_SOURCE[1]##*/}
    echo -e "$(date '+%Y-%m-%dT%H:%M:%S') (${fname}:${BASH_LINENO[0]}:${FUNCNAME[1]}) $*"
}

#================== copy folder, baseline asr_al_random0.0 ==================

#TODO change target fodler name
for i in $(seq 5);do
  if test $i -ge $copy_start
  then
    filename="./asr_al_predictor0.${i}_smoothl1loss_earlystop"
    log "duplicate template into ${filename}"
    cp -rf ./asr_al_random0.0 $filename
  fi
done

sleep 10

# ================== copy folder, baseline asr_al_random0.0 ==================
#TODO change target fodler name
for i in $(seq 5);do
  if test $i -ge $run_start
  then
      filename="./asr_al_predictor0.${i}_smoothl1loss_earlystop"
      log "begin at ${filename}"
      cd $filename || exit

          pwd
          sleep 5

#         generate active learning dataset from dump/raw/train_si284/wav.scp
          python ./active_learning/get_remain_scp.py || exit
          utils/fix_data_dir.sh dump/raw/al_pred
          utils/fix_data_dir.sh dump/raw/train_si284

          printf "train data size:  " && wc -l dump/raw/train_si284/wav.scp

          printf "active learning prediction data size:  " && wc -l dump/raw/al_pred/wav.scp

          if [ -f ./dump/raw/train_si284/wav.scp ] && [ -f ./dump/raw/al_pred/wav.scp ]
          then
             log "run training"
             bash run.sh
          else
            log "cannot find wav.scp"
            exit
          fi

          log "finish training && active leaning prediction"
          python ./active_learning/filter_top10.py || exit

          printf "new data size:  " && wc -l active_learning/selected_wav.scp

      cd ../
      if test $i != 5
      then
        source="${filename}/active_learning/selected_wav.scp"
        target="./asr_al_predictor0.$((i + 1))_smoothl1loss_earlystop/dump/raw/train_si284/wav.scp"
        cp $source $target || exit
        printf "data move succsess:  " && wc -l "./asr_al_predictor0.$((i + 1))_smoothl1loss_earlystop/dump/raw/train_si284/wav.scp"
      fi

      log "finish one iteration"
      echo
      sleep 5
  fi
done
